=== User Registration Conditional Logic ===
Contributors: WPEverest
Tags: user registration, addon, file , user registration conditional logic
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.1.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Conditional logic addon for user registration plugin.

== Description ==

Show or hide fields based upon user input

Get [free support](https://wpeverest.com/support-forum/)


### Features And Options:
* Create interactive forms that react to user’s actions.
* Dynamically show, hide, or change the value of form fields.
* Add or remove list elements on the fly.
* Incredible Support
* Well Documented
* Translation ready

== Installation ==

== Frequently Asked Questions ==

= What is the plugin license? =

* This plugin is released under a GPL license.

= Does the plugin work with any WordPress themes?

Yes, the plugin is designed to work with any themes that have been coded following WordPress guidelines.

== Screenshots ==

== Changelog ==
= 1.0.0 - **/03/2018 =
* Initial release
