<?php
/**
 * UserRegistrationConditionalLogic Admin.
 *
 * @class    URCL_Admin
 * @version  1.0.0
 * @package  UserRegistrationConditionalLogic/Admin
 * @category Admin
 * @author   WPEverest
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * URCL_Admin Class
 */
class URCL_Admin {

	/**
	 * Hook in tabs.
	 */

	public function __construct() {
		urcl_check_plugin_compatibility();
		$message = urcl_is_compatible();
		if ( $message !== 'YES' ) {
			return;
		}
		add_filter( 'user_registration_get_settings_pages', array( $this, 'add_conditional_logic_setting' ), 10, 1 );
		
	}

	function add_conditional_logic_setting( $settings ) {

		if ( class_exists( 'UR_Settings_Page' ) ) {
			
			$settings[] = include( 'settings/class-urcl-settings.php' );
		}
	}
}

return new URCL_Admin();
