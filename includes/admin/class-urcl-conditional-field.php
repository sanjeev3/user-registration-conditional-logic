<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class URCL_Conditional_field {
	
	/**
	 * Hook in tabs.
	 */
	public function __construct() {
		add_action ('user_registration_after_advance_settings', array($this, 'conditional_field') );
	}

	public function conditional_field() {
		
		//$conditional_settings = $this->get_field_conditional_settings();
		
		if ( '' != $conditional_settings ) {

			echo "<div class='ur-advance-setting-block'>";

			echo '<h2>' . __( 'Conditional Settings', 'user-registration-conditional-logic' ) . '</h2>';

			echo '</div>';
		}
	}
}

new URCL_Conditional_field;