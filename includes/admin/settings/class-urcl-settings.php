<?php
/**
 * UserRegistrationConditionalLogic Settings
 *
 * @class    URCL_Settings
 * @version  1.0.0
 * @package  UserRegistrationConditionalLogic/Admin
 * @category Admin
 * @author   WPEverest
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'URCL_Settings ' ) ) :

	/**
	 * URCL_Settings_File Class
	 */
	class URCL_Settings_File extends UR_Settings_Page {

		/**
		 * Constructor.
		 */
		public function __construct() {
			
			$this->id    = 'conditional-logic';
			$this->label = __( 'Conditional Logic', 'user-registration-conditional-logic' );
			add_filter( 'user_registration_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
			add_action( 'user_registration_sections_' . $this->id, array( $this, 'output_sections' ) );
			add_action( 'user_registration_settings_' . $this->id, array( $this, 'output' ) );
			add_action( 'user_registration_settings_save_' . $this->id, array( $this, 'save' ) );
		}

		/**
		 * Get settings
		 *
		 * @return array
		 */

		public function get_settings( $current_section = '' ) {
		
			return apply_filters(
				'user_registration_conditional_logic_settings', array(

					array(
						'title' => __( 'Conditional Logic Settings', 'user-registration-conditional-logic' ),
						'type'  => 'title',
						'desc'  => '',
						'id'    => 'user_registration_conditional_logic_settings',
					),

					array(
						'title'     => __( 'Animation', 'user-registration-conditional-logic' ),
						'desc'      => __( 'Use animations while showing/hiding groups', 'user-registration-conditional-logic' ),
						'id'        => 'user_registration_conditional_logic_animation',
						'type'      => 'select',
						'class'    => 'ur-enhanced-select',
						'css'      => 'min-width: 350px;',
						'desc_tip' => true,
						'options'  => array('Enabled','Disabled'),
					),
					
					array(
						'title'     => __( 'Animation In time', 'user-registration-conditional-logic' ),
						'desc'      => __( 'The time, in milliseconds, takes for each group to show.', 'user-registration-conditional-logic' ),
						'id'        => 'user_registration_conditional_logic_animation_in_time',
						'type'      => 'text',
						'css'      => 'min-width: 350px;',
						'desc_tip' => true,
						'default'  => 200,
					),

					array(
						'title'     => __( 'Animation Out Time', 'user-registration-conditional-logic' ),
						'desc'      => __( 'The time, in milliseconds, takes for each group to hide.', 'user-registration-conditional-logic' ),
						'id'        => 'user_registration_conditional_logic_animation_out_time',
						'type'      => 'text',
						'css'      => 'min-width: 350px;',
						'desc_tip' => true,
						'default'  => 200,
					),
				
				
					array(
						'type' => 'sectionend',
						'id'   => 'user_registration_conditional_logic_settings',
					),
				)
			);

			return apply_filters( 'user_registration_conditional_logic_settings' . $this->id, $settings );
		}


		public function output() {
			
			global $current_section;

			$settings = $this->get_settings( $current_section );

			UR_Admin_Settings::output_fields( $settings );

		}

		/**
		 * Save settings
		*/

		public function save() {

			global $current_section;

			$settings = $this->get_settings( $current_section );

			UR_Admin_Settings::save_fields( $settings );

		}
	}

endif;

return new URCL_Settings_File();