<?php
/**
 * UserRegistrationConditionalLogic Functions.
 *
 * General core functions available on both the front-end and admin.
 *
 * @author   WPEverest
 * @category Core
 * @package  UserRegistrationConditionalLogic/Functions
 * @version  1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @return string
 */
function urcl_is_compatible() {

	$plugins_path = WP_PLUGIN_DIR . URCL_DS . 'user-registration' . URCL_DS . 'user-registration.php';

	if ( ! file_exists( $plugins_path ) ) {

		return __( 'Please install <code>user-registration</code> plugin to use <code>user-registration-conditional-logic</code> addon.', 'user-registration-conditional-logic' );
	}
	$plugin_file_path = 'user-registration/user-registration.php';

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( ! is_plugin_active( $plugin_file_path ) ) {

		return __( 'Please activate <code>user-registration</code> plugin to use <code>user-registration-conditional-logic</code> addon.', 'user-registration-conditional-logic' );

	}
	if ( function_exists( 'UR' ) ) {

		$user_registration_version = UR()->version;

	} else {

		$user_registration_version = get_option( 'user_registration_version' );

	}

	if ( version_compare( $user_registration_version, '1.1.0', '<' ) ) {

		return __( 'Please update your <code>user-registration</code> plugin(to at least 1.1.0 version) to use <code>user-registration-conditional-logic</code> addon.', 'user-registration-conditional-logic' );

	}

	return 'YES';

}

function urcl_check_plugin_compatibility() {

	add_action( 'admin_notices', 'urcl_admin_notices', 10 );

}

function urcl_admin_notices() {

	$class = 'notice notice-error';

	$message = urcl_is_compatible();

	if ( 'YES' !== $message ) {

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), ( $message ) );
	}
}
